import { Link } from 'react-router-dom';
import './styles.css';
type Props = {
    text:string;
    link:string;
}
const ButtonGeral = ({text,link}:Props) =>{
    return (
        <Link to ={link}className="button-geral"><h5>{text}</h5></Link>
    );
}
export default ButtonGeral;