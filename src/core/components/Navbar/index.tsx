import './styles.css';
import {Link} from 'react-router-dom';
const Navbar = () =>{
    return (
        <nav className="main-navbar">
            <Link to="/" className="logo-text">BootCamp DevSuperior</Link>
        </nav>
    );
}
export default Navbar;