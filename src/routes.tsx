import { BrowserRouter, Route, Switch } from "react-router-dom";
import Navbar from "./core/components/Navbar";
import Home from "./pages/Home";
import SearchGit from "./pages/SearchGit";

const Routes = () => (
    <BrowserRouter>
        <Navbar/>
        <Switch>
            <Route path="/" exact>
                <Home/>
            </Route>
            <Route path="/SearchGit">
                <SearchGit/>
            </Route>
        </Switch>
    </BrowserRouter>
);
export default Routes;