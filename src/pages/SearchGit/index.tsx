import axios from 'axios';
import React, { useState } from 'react';
import ButtonGeral from '../../core/components/ButtonGeral';
import ImageLoader from '../../core/components/Loaders/ImageLoader';
import './styles.css';
import { Github } from './types';
const API_URL = 'https://api.github.com/users/';

const SearchGit = () => {
    const [searchValue, setSearchValue] = useState('');
    const [gitData, setGitData] = useState<Github>();
    const [isLoading, setIsLoading] = useState(false);

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        setIsLoading(true);
        event.preventDefault();
        setGitData(undefined);
        axios(`${API_URL}${searchValue}`)
            .then(response => setGitData(response.data))
            .catch(() => console.log('erro ao buscar!'))
            .finally(() => setIsLoading(false))


    }
    return (
        <>
            <div className="border-search">
                <form className="container" onSubmit={handleSubmit}>
                    <div className="title">
                        <h3>Encontre um perfil Github</h3>
                    </div>
                    <input type="text" placeholder="Usuario github"
                        value={searchValue}
                        className="input"
                        onChange={event => setSearchValue(event.target.value)} />
                    <button className="button-geral">Buscar</button>
                </form>
            </div>

            {isLoading ? <div className="container-teste"><ImageLoader /> </div>: gitData && (
                <div className="container-teste">


                    <div className="search-result-item-foto">
                        <img className="result-img" src={gitData.avatar_url} />
                        <a className="button-geral margin-shit" href={gitData.html_url}>Ver Perfil</a>
                    </div>
                    <div className="content-tudo">
                        <div className="sub-info">
                        <span className="sub-infosuperior">{"Repositórios Públicos: "+ gitData.public_repos}</span>
                        <span className="sub-infosuperior">{"Seguidores: "+ gitData.followers}</span>
                        <span className="sub-infosuperior">{"Seguindo: " + gitData.following}</span>
                        </div>
                        <div className="informacoes">

                            <div>
                                <h1 className="informacoes-title">Informações</h1>
                            </div>
                            <div className="search-result-item-texto">
                                <strong className="result-title">Empresa: </strong>
                                <span className="result-subtitle">{gitData.company !== null ? gitData.company : '  não possui empresa'}</span>
                            </div>
                            <div className="search-result-item-texto">
                                <strong className="result-title">WebSite/Blog: </strong>
                                <span className="result-subtitle">{gitData.blog !== '' ? gitData.blog : '  não possui blog'}</span>
                            </div>
                            <div className="search-result-item-texto">
                                <strong className="result-title">Localização: </strong>
                                <span className="result-subtitle">{gitData.location !== null ? gitData.location : 'não possui localização'}</span>
                            </div>
                            <div className="search-result-item-texto">
                                <strong className="result-title">Empresa: </strong>
                                <span className="result-subtitle">{gitData.company !== null ? gitData.company : 'não possui empresa'}</span>
                            </div>
                        </div>
                    </div>
                </div>
            )}


        </>
    );
}
export default SearchGit;