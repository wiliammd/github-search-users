export type Github = {
    public_repos:number;
    followers:number;
    following:number;
    company:string;
    blog:string;
    location:string;
    created_at:string;
    html_url:string;
    avatar_url:string;
}